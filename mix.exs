defmodule ECPay.MixProject do
  use Mix.Project

  def project do
    [
      app: :ecpay,
      version: "1.0.1",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package(),
      description: description()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      env: default_config()
    ]
  end

  defp description do
    """
    ## ECPay 綠界 Payment Gateway Integration for Elixir

    The purpose of this library is to provide API integration with the ECPay (綠界科技) payment
    gateway provided by the Taiwan-based Green World FinTech Service Co., Ltd. (綠界科技股份有限公司).
    """
  end

  defp package do
    [
      files: ["lib", "mix.exs", "README*", "LICENSE*"],
      maintainers: ["Karol Moroz"],
      licenses: ["BSD-3"],
      links: %{"GitLab" => "https://gitlab.com/moroz2137/ecpay_elixir"}
    ]
  end

  defp default_config do
    [
      merchant_id: "2000132",
      hash_key: "5294y06JbISpM5x9",
      hash_iv: "v77hoKGq4kWxNNIS",
      staging: true,
      return_url: "https://www.ecpay.com.tw/receive.php",
      ignored: "AndroidPay#WebATM#CVS#BARCODE",
      expiration_period: 1
    ]
  end

  defp deps do
    [
      {:timex, "~> 3.5.0"},
      {:phoenix_html, "~> 2.13"},

      # Hex
      {:ex_doc, ">= 0.0.0", only: :dev}
    ]
  end
end
