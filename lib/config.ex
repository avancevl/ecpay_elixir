defmodule ECPay.Config do
  @config_vars [:merchant_id, :hash_key, :hash_iv, :return_url, :ignored, :expiration_period]

  for var <- @config_vars do
    def unquote(var)() do
      Application.get_env(:ecpay, unquote(var))
    end
  end

  def prod? do
    Application.get_env(:ecpay, :prod) == true
  end

  def payment_url do
    if prod?() do
      "https://payment.ecpay.com.tw/Cashier/AioCheckOut/V5"
    else
      "https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V5"
    end
  end
end
