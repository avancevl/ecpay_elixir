defmodule ECPay.Checksum do
  alias ECPay.AIOParams, as: Params
  alias ECPay.Config

  @spec calculate(params :: ECPay.AIOParams.t() | map()) :: String.t()
  def calculate(%Params{} = params) do
    Params.to_map(params)
    |> calculate()
  end

  def calculate(params) when is_map(params) do
    params
    |> encode_map_as_query()
    |> uri_escape()
    |> hash()
  end

  def verify(%{"CheckMacValue" => checksum} = params) do
    case calculate(params) == checksum do
      true ->
        :ok

      false ->
        {:error, :checksum}
    end
  end

  def uri_escape(string) do
    string
    |> URI.encode_www_form()
    |> String.downcase()
  end

  @forbidden ["CheckMacValue", "HashKey", "HashIV"]
  @spec encode_map_as_query(map()) :: binary()
  def encode_map_as_query(map) when is_map(map) do
    hash_key = Map.get(map, "HashKey", Config.hash_key())
    hash_iv = Map.get(map, "HashIV", Config.hash_iv())

    query_str =
      Enum.filter(map, fn {k, v} -> !is_nil(v) && k not in @forbidden end)
      |> Enum.map(fn {key, val} -> "#{key}=#{val}" end)
      |> Enum.join("&")

    "HashKey=#{hash_key}&" <> query_str <> "&HashIV=#{hash_iv}"
  end

  defp hash(value), do: :crypto.hash(:sha256, value) |> Base.encode16(case: :upper)
end
