defmodule ECPay.HTML do
  import Phoenix.HTML.Tag
  alias ECPay.{AIOParams, Config}

  def render_to_iodata(%AIOParams{} = params) do
    params
    |> AIOParams.to_form_data()
    |> render_to_iodata
  end

  def render_to_iodata(params) when is_map(params) do
    content_tag :form, action: Config.payment_url(), method: "post" do
      for {key, value} <- params do
        tag(:input, type: "hidden", name: key, id: key, value: value)
      end
    end
  end
end
