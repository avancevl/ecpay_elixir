defmodule ECPay.AIOParams do
  alias ECPay.{Config, AIOParams, Checksum}

  defstruct transaction_no: nil,
            timestamp: nil,
            amount: 1,
            description: nil,
            item_name: nil,
            payment: "ALL",
            language: nil

  def to_form_data(%AIOParams{} = params) do
    map = to_map(params)
    mac_value = Checksum.calculate(map)
    Map.put(map, "CheckMacValue", mac_value)
  end

  def to_map(%AIOParams{} = params) do
    base = %{
      "EncryptType" => 1,
      "MerchantID" => Config.merchant_id(),
      "MerchantTradeNo" => params.transaction_no,
      "MerchantTradeDate" => normalize(params.timestamp),
      "PaymentType" => "aio",
      "TotalAmount" => params.amount,
      "TradeDesc" => params.description,
      "ItemName" => params.item_name,
      "ReturnURL" => Config.return_url(),
      "ChoosePayment" => params.payment,
      "IgnorePayment" => Config.ignored(),
      "ExpireDate" => Config.expiration_period()
    }

    base
    |> maybe_set_language(params.language)
  end

  defp maybe_set_language(params, nil), do: params

  defp maybe_set_language(params, other) when is_binary(other),
    do: Map.put(params, "Language", other)

  defp normalize(%NaiveDateTime{} = time) do
    time |> Timex.to_datetime("Etc/UTC") |> Timex.to_datetime("Asia/Taipei") |> format
  end

  defp normalize(%DateTime{} = time) do
    time |> Timex.to_datetime("Asia/Taipei") |> format
  end

  defp format(timestamp), do: Timex.format!(timestamp, "%Y/%m/%d %H:%M:%S", :strftime)
end
